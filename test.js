#testjsfile
const { Builder, By, Key, until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
 
(async function example() {
  // Configure Chrome WebDriver options
  const options = new chrome.Options();
  options.addArguments('--headless'); // Run Chrome in headless mode (without GUI)
 
  // Initialize Chrome WebDriver
  const driver = await new Builder()
    .forBrowser('chrome')
    .setChromeOptions(options)
    .build();
 
  try {
// Navigate to Google.com
await driver.get('https://www.google.com');
 
    // Wait until the search input field is located
await driver.wait(until.elementLocated(By.name('q')), 10000);
 
    // Find the search input field and enter a search query
await driver.findElement(By.name('q')).sendKeys('OpenAI', Key.RETURN);
 
    // Wait until search results are displayed
    await driver.wait(until.titleContains('OpenAI'), 10000);
 
    // Get the title of the search results page and print it to the console
    const title = await driver.getTitle();
    console.log('Page title:', title);
  } finally {
    // Quit the WebDriver session
    await driver.quit();
  }
})();